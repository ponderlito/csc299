from fabric.api import *
from fabric.operations import put, get
import os
import zipfile as ZipFile

def set_hosts():
    env.hosts = ['localhost']
    env.user   = "csc299_1837068_test"

def get_all_file_paths():
 
    # initializing empty file paths list
    file_paths = []
 
    # crawling through directory and subdirectories
    for root, directories, files in os.walk(os.getcwd()):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
 
    # returning all file paths
    return file_paths  

def zipFolder():
	with open('csc299.zip','w') as zip:
        # writing each file one by one
	        for file in get_all_file_paths():
        	    zip.write(file)



def unzip():
	with zipfile.ZipFile("csc299.zip","r") as zip_ref:
	    	zip_ref.extractall("~/")

def putRemote():
	set_hosts()
	put("~/csc299/csc299.zip", "~/")

def makeFolder():
	run('mkdir csc299/lab08')

def downloadThing():
	run('wget http://web2py.com/examples/static/web2py-src.zip -P ~/csc299/lab08')
	run('unzip ~/csc299/lab08/web*')
	run('python ~/csc299/lab08/*.py -p 7068 -i 0.0.0.0')

zipFolder()
putRemote()
unzip()
makeFolder()
downloadThing()
