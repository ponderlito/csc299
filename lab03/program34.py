import sys

accounts = ''
totals = []
with open ('totals.csv','r') as tot:
	totals = tot.readlines()

with open('accounts.csv','r') as acc:
	accounts = acc.readlines()
	with open ('results.csv','w') as outfile:
		header = accounts[0].strip() + ',' +totals[0].split(',')[1].strip()
		outfile.write(header)
		outfile.write('\n')
		for l in totals[1:]:
			for a in accounts[1:]:
				if l.split(',')[0] ==a.split(',')[0]:
					money = l.split(',')[1].strip().strip('$')
					money = float(money)
					other = a.split(',')[3].strip('$').strip()
					other = float(other)
					mytotal = other - money
					outfile.write( ''.join(a).strip() +',' + '${}'.format(money))
					outfile.write('\n')
