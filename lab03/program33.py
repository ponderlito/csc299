import csv
import sys

people = {} 
with open('expenses.csv', 'rU') as csvfile:
	dialect = csv.Sniffer().sniff(csvfile.read(1024))
	csvfile.seek(0)
	reader = csv.reader(csvfile, dialect)
	
	temp = []
	for line in reader:
		temp.append(line)

	for line in temp[1:]:
		people[line[0]] = 0
	for line in temp[1:]:
		people[line[0]]= float(line[1].strip('$'))
		#people [line[0]] += line [1]
	

	with open('totals.csv','w') as outfile:
		outfile.write('USER_ID, TOTAL_EXPENSE\n')
		for person in people:
			outfile.write('{}, ${}\n'.format(person,people[person]))
