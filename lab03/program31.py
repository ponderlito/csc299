import requests
import json
import sys

url = "http://mdp.cdm.depaul.edu/csc299/static/data/1837068.expenses.csv"
url2 = 'http://mdp.cdm.depaul.edu/csc299/static/data/1837068.accounts.csv'


def download(url,out):
	r = requests.get(url)
	contents = r.text.split('\n')
	writeout(contents,out)


def writeout(contents,out):
	with open(out, 'w')as outfile:
		def writeRow(row):
			thing = ''
			for item in row[:-1]:
				thing += '{},'.format(item)
			thing += row[-1]
			outfile.write(thing+'\n')
		for e in contents:
			writeRow(e.split(','))

download(url2,'accounts.csv')
download(url, 'expenses.csv')		
