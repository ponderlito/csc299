Name: <Pablo Alguinidigue>
StudentId: <1837068>
Email: <pablo.alguindigue@gmail.com>

Operating System

uname -v
Kernel Version: #49~14.04.1-Ubuntu

uname
Hardware name: Linux 

uname -p
Processor name: x86_64

uname -o
Operating System: GNU/Linux 

ls command

which ls 
ls location: /bin

ls ls -l
/bin/ls size: 110080

man ls
Authors: Richard M. Stallman and David MacKenzie.

ls -a
Hidden files: ls -a

file system

/home: 	
A home directory, also called a login directory, is the directory on Unix-like operating systems that serves as the repository for a user's personal files, directories and programs. It is also the directory that a user is first in after logging into the system.

http://www.linfo.org/home_directory.html

/bin:
It contains essential binary files (unlike /usr/bin directory) also for booting. It usually contains the shells like bash and commonly used commands like cp, mv, rm, cat, ls. 
Unlike /sbin, the bin directory contains several useful commands that are of use to both the system administrator as well as non-privileged users.

https://unix.stackexchange.com/questions/237152/purpose-of-bin-directory

/var:
/var is a standard subdirectory of the root directory in Linux and other Unix-like operating systems that contains files to which the system writes data during the course of its operation.

http://www.linfo.org/var.html

/etc:
this is the nerve center of your system, it contains all system related configuration files in here or in its sub-directories.

https://www.tldp.org/LDP/Linux-Filesystem-Hierarchy/html/etc.html

CROSSWORD:

ACROSS:

2: passwd
7: root
8: rmdir
9: mounting
11: relative

DOWN:

1:cp
3: absolute
4:whoami
5: uname
6: terminal
10: less
11: rm
