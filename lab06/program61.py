from bs4 import BeautifulSoup as soup
import sys, requests, csv
url = 'https://www.superherodb.com/characters'

r = requests.get(url, verify=False)

heroes = []

if r.status_code == 200:
	thesoup = soup(r.text, 'html.parser')
	for x in thesoup.findAll('li'):
		thing = x.findAll('a')[0].get('title')
		if thing and not thing.startswith('List'):
			heroes.append( thing)

heroes = heroes[:-2]


with open('names.csv', 'w') as outfile:
	spamwriter = csv.writer(outfile)
	spamwriter.writerow(['name'])
	for person in heroes:
		spamwriter.writerow([person])
    
