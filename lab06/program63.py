from bs4 import BeautifulSoup as soup
import sys, requests, re, csv
url = 'https://www.superherodb.com/characters/#A'
base = 'https://www.superherodb.com/characters/'


r = requests.get(url, verify=False)
if r.status_code == 200:
        thesoup = soup(r.text, 'html.parser')
	with open('superheroes.csv', 'w') as outfile:
        	spamwriter = csv.writer(outfile)
        	spamwriter.writerow(['NAME','INTELLIGENCE','STRENGTH','SPEED','DURABILITY','POWER','COMBAT'])
		for link in thesoup.findAll('a',attrs={'title':re.compile('\w+')})[0:10]:
			url = base+link['href']
		
			r = requests.get(url, verify=False)
			if r.status_code == 200:
				hero ={'Name':'','Intelligence' :'','Strength':'','Speed':'', 'Durability':'','Power':'', 'Combat':''}
        			thesoup2 = soup(r.text, 'html.parser')
				namelookup = thesoup2.find('div',attrs={'class':'cblock titleprofile'})
        			hero['Name'] = namelookup.h1.text.strip()
		

				for block in thesoup2.findAll('div', attrs={'class':'cblock'}):
                			if block.h3:
                        			if block.h3.string == 'Powerstats':
                                			b = block.findAll('div',attrs={'class':'gridbarholder'})


                                			for x in b:
                                        			cat = x.find('div',attrs={'class':'gridbarlabel'}).text.strip()
                                        			val = x.find('div',attrs={'class':'gridbarvalue'}).text.strip()
                                        			hero[cat] = val


       							spamwriter.writerow( [hero['Name'],hero['Intelligence'],hero['Strength'],hero['Speed'], hero['Durability'], hero['Power'], hero['Combat'] ])

