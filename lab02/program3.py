import os, sys

mylst = os.listdir('/bin')

with open("bincount.csv", 'w') as outfile:
        for i in range(1,20):
                count = 0
                for f in mylst:
                        if len(f) == i:
                                count = count + 1
                outfile.write('{}, {}\n'.format(i, count))
