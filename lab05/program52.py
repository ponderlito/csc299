from bs4 import BeautifulSoup as soup
import requests, re, sys, json

url ='http://odata.cdm.depaul.edu/Cdm.svc/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20%27A%27%20and%20SubjectId%20eq%27CSC%27'
r = requests.get(url)
thesoup = soup(r.text, 'html.parser')
thesoup = thesoup.find('content')
thesoup = thesoup.find('m:properties')
thesoup = list(thesoup.findAll(name=re.compile('^d\:\w+')))

mydict = {}
for i  in thesoup:
	key = str(i).split('</d:')[1]
	content = str(i).split('</d:')[0].split('>')[-1]
	mydict[key] = content

with open('CSC.properties.1.json','w') as outfile:

	json.dump(mydict,outfile)
