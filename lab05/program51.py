from bs4 import BeautifulSoup as soup
import requests


url ='http://odata.cdm.depaul.edu/Cdm.svc/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20%27A%27%20and%20SubjectId%20eq%27CSC%27'
r = requests.get(url)
thesoup = soup(r.text, 'html.parser')
thesoup = str(thesoup.find('content')).split('\n')

with open ('CSC.xml', 'w') as outfile:
	for i in thesoup[1:-1]:
		outfile.write(i)
		outfile.write('\n')	
	
