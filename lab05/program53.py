from bs4 import BeautifulSoup as soup
import requests, re, sys, json

url ='http://odata.cdm.depaul.edu/Cdm.svc/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20%27A%27%20and%20SubjectId%20eq%27CSC%27'
r = requests.get(url)
thesoup = soup(r.text, 'html.parser')
thesoup = thesoup.findAll('m:properties')
dictlist = []

for i in thesoup:
	mydict ={}
	for j in str(i).split('\n')[1:-1]:
		key = j.split('</d:')[-1][:-1]
		content = j.split('</d:')[0].split('>')[-1]
		mydict[key] = content
	dictlist.append(mydict)


with open('CSC.json', 'w') as outfile:
	json.dump(dictlist, outfile)
